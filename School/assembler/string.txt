.text 
NIZ1: .asciz "Danes je lep dan" 
NIZ2: .space 17 
crka: .byte 0x6a  @crka "j" 

.align  
.global __start 

__start: 
        adr r0,NIZ1 
        adr r1,crka 
        ldrb r2,[r1]        @r1 <- "j" 
         
loop:   ldrb r4,[r0,r3]     @r4 <- znak iz niza 1, r3 iterira po tekstu 
        cmp r4,r2           @preveri ali je znak enak j  
        addne r3,r3,#1      @ce ni iteriraj na naslednji znak 
        bne loop            @in pojdi nazaj na loop 
         
        adr r5,NIZ2         @ko najde j 
        strb r4,[r5,r6]     @ga zapise v prvi bajt niza2 
        add r3,r3,#1        @iteriramo na naslednjo crko 
        mov r6,#1           @in na naslednjo lokacijo v NIZ2 
 
 
loop2:  ldrb r4,[r0,r3]     @r4 <- znak iz niza2, r3 se vedno iterira 
        cmp r4,r14          @preveri da ni konec niza 
        strneb r4,[r5,r6]   @in ga zapise v niz2+r6 
        addne r6,r6,#1      @iterira v naslednji naslov 
        addne r3,r3,#1      @iterira na naslednjo crko 
        bne loop2 
 
__end:    b __end