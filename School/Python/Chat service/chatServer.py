import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)
import socket
import struct
import threading
import ssl


PORT = 1234
HEADER_LENGTH = 2



def receive_fixed_length_msg(sock, msglen):
	message = b''
	while len(message) < msglen:
		chunk = sock.recv(msglen - len(message)) # preberi nekaj bajtov
		if chunk == b'':
			raise RuntimeError("socket connection broken")
		message = message + chunk # pripni prebrane bajte sporocilu

	return message

def receive_message(sock):
	header = receive_fixed_length_msg(sock, HEADER_LENGTH) # preberi glavo sporocila (v prvih 2 bytih je dolzina sporocila)
	message_length = struct.unpack("!H", header)[0] # pretvori dolzino sporocila v int

	message = None
	if message_length > 0: # ce je vse OK
		message = receive_fixed_length_msg(sock, message_length) # preberi sporocilo
		message = message.decode("utf-8")

	return message

def send_message(sock, message):
	encoded_message = message.encode("utf-8") # pretvori sporocilo v niz bajtov, uporabi UTF-8 kodno tabelo

	# ustvari glavo v prvih 2 bytih je dolzina sporocila (HEADER_LENGTH)
	# metoda pack "!H" : !=network byte order, H=unsigned short
	header = struct.pack("!H", len(encoded_message))

	message = header + encoded_message # najprj posljemo dolzino sporocilo, slee nato sporocilo samo
	sock.sendall(message);

# funkcija za komunikacijo z odjemalcem (tece v loceni niti za vsakega odjemalca)
def client_thread(client_sock, client_addr):
	global clients, usrnm_sock

	print("[system] connected with " + client_addr[0] + ":" + str(client_addr[1]))
	print("[system] we now have " + str(len(clients)) + " clients")

	while True: # neskoncna zanka
		msg_received = receive_message(client_sock)

		if not msg_received: # ce obstaja sporocilo
			break

		cert = client_sock.getpeercert()
		for sub in cert['subject']:
			for key, value in sub:
				# v commonName je ime uporabnika
				if key == 'commonName':
					user = value


		data = msg_received.split("###")
		message = data[0]
		# t = data[1]  # not used, needed to be sent to the server for some reason though
		sender = user
		recipient = data[2]

		messag = "{} said: {}".format(sender, message)

		if message == "Hello server!":
			usrnm_sock[sender] = client_sock


		print("[RKchat] [" + client_addr[0] + ":" + str(client_addr[1]) + "] : " + msg_received)

		if recipient == "global":
			for client in clients:
				send_message(client, messag.upper())
		elif recipient == "server":
			send_message(client_sock, "Server connecting your username and socket..s.")
		elif recipient in usrnm_sock:
			send_message(usrnm_sock[recipient], messag) # send message to specific user
		else:
			send_message(client_sock, "User not online.") # only if he is online


	# prisli smo iz neskoncne zanke
	with clients_lock:
		clients.remove(client_sock)
	print("[system] we now have " + str(len(clients)) + " clients")
	client_sock.close()

def setup_SSL_context():

	#uporabi samo TLS, ne SSL
	context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
	# certifikat je obvezen
	context.verify_mode = ssl.CERT_REQUIRED
	#nalozi svoje certifikate
	context.load_cert_chain(certfile="server.pem", keyfile="serverkey.pem")
	# nalozi certifikate CAjev, ki jim zaupas
	# (samopodp. cert. = svoja CA!)
	context.load_verify_locations('clients.pem')
	# nastavi SSL CipherSuites (nacin kriptiranja)
	context.set_ciphers('AES128-SHA')
	return context

my_ssl_ctx = setup_SSL_context()

# kreiraj socket
server_socket = my_ssl_ctx.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM), server_side=True)
server_socket.bind(("localhost", PORT))
server_socket.listen(1)

#ime uporabnika dobimo iz certifikata


# cakaj na nove odjemalce
print("[system] listening ...")
clients = set()
usrnm_sock = {} # connecting usernames and sockets
clients_lock = threading.Lock()
while True:
	try:
		# pocakaj na novo povezavo - blokirajoc klic
		client_sock, client_addr = server_socket.accept()

		with clients_lock:
			clients.add(client_sock)

		thread = threading.Thread(target=client_thread, args=(client_sock, client_addr))
		thread.daemon = True
		thread.start()

	except KeyboardInterrupt:
		break

print("[system] closing server socket ...")
server_socket.close()
