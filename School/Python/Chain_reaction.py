#__author__ = 'Luka Prebil Grintal'
import risar, random, math

x = random.randint(-5, 5)
speed = [x, math.sqrt(25 - x**2)]
radius = 10

time = 0

x, y = risar.nakljucne_koordinate()
barva = risar.nakljucna_barva()
krog = risar.krog(x, y, radius, barva)
if x <= radius or y <= radius:
    x += radius
    y += radius
if x >= risar.maxX - radius or y >= risar.maxY - radius:
    x -= radius
    y -= radius

while time < 20:
    if x >= risar.maxX - radius or x <= radius:
        speed[0] *= -1
    if y >= risar.maxY - radius or y <= radius:
        speed[1] *= -1
    x, y = x + speed[0], y + speed[1]
    risar.odstrani(krog)
    krog = risar.krog(x, y, radius, barva)
    risar.cakaj(1/60)
    time += 1/60
