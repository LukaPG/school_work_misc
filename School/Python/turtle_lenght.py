#__author__ = 'Luka Prebil Grintal'

class User(object):

    def __init__(self):
        self.id = None
        self.name = None
        self.ships = ["""Im inclined on having ships be class objects with their own properties like engine, crew..."""]
        self.whatever = "whatever one wants"

    def getID(self):
        return self.id

    def getName(self):
        return self.name

    def getShips(self):
        return self.ships

    #or

    def getShips(self):
        for ship in self.ships:
            yield ship

    def getWhatever(self):
        return self.whatever

    def doStuffWithUserProperties(self):
        foo