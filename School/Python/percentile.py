#__author__ = 'Luka Prebil Grintal'
import math

fn = input("Filename (must be in the same dir as this script): ")
file = open(fn)
list = []
perc = int(input("What percentile of the data do you want? "))

for line in file:
    list.append(float(line))

list.sort()

def percentile(data, percentile):
    size = len(data)
    return sorted(data)[int(math.ceil((size * percentile) / 100)) - 1]

print(percentile(list, perc))

