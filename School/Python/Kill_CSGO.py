#__author__ = 'Luka Prebil Grintal'
import math, time, psutil
from subprocess import check_output
name = "Apoint.exe"


def isRunning(prName):
    return True if prName in [psutil.Process(pid).name() for pid in psutil.pids()] else False

def get_pid(name):
    return check_output(["pidof",name])

def runtime(prName):
    starttime = psutil.Process(get_pid(prName)).create_time()
    return time.time() - starttime

print(isRunning(name), get_pid(name), runtime(name))
