import java.io.File;
import java.util.*;


public class DN04{
	
	public static void main(String[] args) throws Exception {
		//TODO if args
		String ime = args[0];
		
		Scanner dat = new Scanner(new File(ime));
		
		while (dat.hasNextLine()){
			String vrstica = dat.nextLine();
			vrstica = vrstica.trim();
			String[] besede = vrstica.split("\\s+");
			int dolzina = besede.length;
			if ((dolzina == 1) && (besede[0].equals(""))){
				dolzina = 0;
			}
			System.out.printf("%d\n", dolzina);
		}
		
	}
	
}