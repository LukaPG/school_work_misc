import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;

public class DN06{
	
	static void case1(String path){
		File f = new File(path);
		//System.out.println(path);
		String[] folders = path.split("/");
		//System.out.println(folders);
		String filename = folders[folders.length-1];
		//System.out.println(filename);
		long lastmod = f.lastModified();  //ms since epoch
		long size = f.length();
		
		SimpleDateFormat fr = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
		Date dat = new Date(lastmod);   //converts to human readable
		String dt = fr.format(dat);
		
		path = path.substring(0, path.length()-(filename.length()+1));
		//System.out.println(path);
		System.out.println("Podatki o datoteki:");
		System.out.printf("- ime: %s\n", filename);
		System.out.printf("- pot: %s\n", path);
		System.out.printf("- velikost: %d bajtov\n", size);
		System.out.printf("- spremenjena: %s\n", dt);	
	}
	
	
	static void case2(String path1, String path2){
		File f1 = new File(path1);
		File f2 = new File(path2);
		
		String[] folders = path1.split("/");
		String name1 = folders[folders.length-1];
		String[] folders2 = path2.split("/");
		String name2 = folders2[folders2.length-1];
		
		long size1 = f1.length();
		long size2 = f2.length();
		
		long lastmod1 = f1.lastModified(); //ms since epoch
		long lastmod2 = f2.lastModified();
		
		SimpleDateFormat fr = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
		Date dat1 = new Date(lastmod1);			//converts to human readable
		Date dat2 = new Date(lastmod2);
		String dt1 = fr.format(dat1);
		String dt2 = fr.format(dat2);
		
		
		if (!(name1.equals(name2))){
			System.out.println("Datoteki se razlikujeta v imenu:");
			System.out.printf("(1) %s\n", name1);
			System.out.printf("(2) %s\n", name2);
		}
		if (size1 != size2){
			System.out.println("Datoteki se razlikujeta v velikosti:");
			System.out.printf("(1) %d bajtov\n", size1);
			System.out.printf("(2) %d bajtov\n", size2);
		}
		if (!(dat1.equals(dat2))){
			System.out.println("Datoteki se razlikujeta v datumu zadnje spremembe:");
			System.out.printf("(1) %s\n", dt1);
			System.out.printf("(2) %s\n", dt2);
		}
		if (name1.equals(name2) && size1 == size2 && dat1.equals(dat2)){
			System.out.println("Datoteki sta enaki:");
			System.out.printf("(1) %s\n", path1);
			System.out.printf("(2) %s\n", path2);
		}
		
	}
	
	
	static void case3(String path) throws Exception { //checksum
		Scanner sc = new Scanner(new File(path)).useDelimiter("");
		int cksum = 0;
		
		while (sc.hasNext()){
			String chr = sc.next();
			char ch = chr.charAt(0);
			cksum += (int) ch;
		}
		
		cksum %= 1024;
		System.out.printf("Kontrolna vsota datoteke je: %d\n", cksum);
	}
	
	
	static void case4(String path1, String path2)throws Exception{ //razlika datotek v znaku
		Scanner sc1 = new Scanner(new File(path1)).useDelimiter("");
		Scanner sc2 = new Scanner(new File(path2)).useDelimiter("");
		
		int chnum = 0;
		while (sc1.hasNext() && sc2.hasNext()){
			char ch1 = sc1.next().charAt(0);
			char ch2 = sc2.next().charAt(0);

			if (ch1 == ch2) chnum++;
			else {
				System.out.printf("Datoteki se razlikujeta v vsebini pri znaku: %d\n", chnum+1);
				break;
			}
			
			
		}
		if (chnum == 0) System.out.printf("Datoteki se razlikujeta v vsebini pri znaku: %d\n", chnum);
		else if (!(sc1.hasNext()) || !(sc2.hasNext())){
			System.out.println("Datoteki imata enako vsebino.");
		}
	}
	
	//static List<File> filesAndSubdirs = new ArrayList<File>();
	
	static void case5(String path){
		File f = new File(path);
        File[] subdirs = f.listFiles();
		
        for (File file : subdirs) {
            if (file.isDirectory()) {
				System.out.println(file);
                case5(file.getAbsolutePath());
            } 
			else if (file.isFile()) {
				System.out.println(file);
            }
        }
		
	}
	
	
	static void case6(String path){
		File f = new File(path);
		File[] files = f.listFiles();
		
		for (File file : files){
			if (file.isFile()){
				System.out.println(file);
			}
			else if (file.isDirectory()){
				System.out.println(file);
				File[] fls = file.listFiles();
				for (File fl : fls){
					System.out.println(fl);
				}
			}
		}
	}
	
	
	static void case7(){}
	
	public static void main(String[] args) throws Exception {
		int nacin = Integer.parseInt(args[0]);
		
		switch (nacin){
			case 1: case1(args[1]           ); break;
			case 2: case2(args[1], args[2]); break;
			case 3: case3(args[1]           ); break;
			case 4: case4(args[1], args[2]); break;
			case 5:
				System.out.printf("Vsebina direktorija %s:\n", args[1]);
				case5(args[1]); 
				break;
			case 6:
				System.out.printf("Vsebina direktorija %s:\n", args[1]);
				case6(args[1]); 
				break;
			case 7: case7(); break;
		}
	}
	
}