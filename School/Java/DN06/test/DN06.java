import java.io.*;
import java.util.*;

public class DN06{
	
	static void case1(String path){
		File f = new File(path);
		System.out.println(path);
		String[] folders = path.split("/");
		System.out.println(folders);
		String filename = folders[folders.length-1];
		System.out.println(filename);
		long lastmod = f.lastModified();  //ms since epoch
		long size = f.length();
		Date dt = new Date(lastmod);   //converts to human readable
		
		path = path.substring(0, path.length()-filename.length());
		System.out.println(path);
		System.out.println("Podatki o datoteki:");
		System.out.printf("- ime: %s\n", filename);
		System.out.printf("- pot: %s\n", path);
		System.out.printf("- velikost: %d bajtov\n", size);
		System.out.printf("- spremenjena: %s\n", dt);	
	}
	
	static void case2(String path1, String path2){
		File f1 = new File(path1);
		File f2 = new File(path2);
		
		String[] folders = path1.split("/");
		String name1 = folders[folders.length-1];
		String[] folders2 = path2.split("/");
		String name2 = folders2[folders2.length-1];
		
		long size1 = f1.length();
		long size2 = f2.length();
		
		long lastmod1 = f1.lastModified(); //ms since epoch
		long lastmod2 = f2.lastModified();
		Date dt1 = new Date(lastmod1);			//converts to human readable
		Date dt2 = new Date(lastmod2);
		
		if (!(name1.equals(name2))){
			System.out.println("Datoteki se razlikujeta v imenu:");
			System.out.printf("(1) %s\n", name1);
			System.out.printf("(2) %s\n", name2);
		}
		if (size1 != size2){
			System.out.println("Datoteki se razlikujeta v velikosti:");
			System.out.printf("(1) %d bajtov\n", size1);
			System.out.printf("(2) %d bajtov\n", size2);
		}
		if (!(dt1.equals(dt2))){
			System.out.println("Datoteki se razlikujeta v datumu zadnje spremembe:");
			System.out.printf("(1) %s\n", dt1);
			System.out.printf("(2) %s\n", dt2);
		}
		if (name1.equals(name2) && size1 == size2 && dt1.equals(dt2)){
			System.out.println("Datoteki sta enaki:");
			System.out.printf("(1) %s\n", path1);
			System.out.printf("(2) %s\n", path2);
		}
		
	}
	
	
	static void case3(){}
	static void case4(){}
	static void case5(){}
	static void case6(){}
	static void case7(){}
	
	public static void main(String[] args){
		int nacin = Integer.parseInt(args[0]);
		
		switch (nacin){
			case 1: case1(args[1]); break;
			case 2: case2(args[1], args[2]); break;
			case 3: case3(); break;
			case 4: case4(); break;
			case 5: case5(); break;
			case 6: case6(); break;
			case 7: case7(); break;
		}
	}
	
}