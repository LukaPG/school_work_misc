import java.util.*;

public class Predavanja03{
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		
		int ocena;
		int vsota = 0;
		int n = -1;
		do {
			System.out.print("Vpisi oceno: ");
			ocena = sc.nextInt();
			vsota = vsota + ocena;
			n = n+1;
		} while (ocena != 0);
		
		if (n != 0){
			double avg = (double) vsota / n;
			System.out.printf("Povprecje %d prebranih ocen je %.2f\n", n, avg);
		}
		else{
			System.out.println("Niste vpisali nobene ocene");
		}	
	}
}