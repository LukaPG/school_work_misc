package com.company;
import java.lang.String;

public class DN01 {

    public static void main(String[] args) {
        String ime = "Moje ime je: Luka Prebil Grintal";
        String vpisna = "Moja vpisna številka: 63150371";
        String izjava = "**************************************************\n" +
                        "* Izjavljam, da sem prebral vsebino strani       *\n" +
                        "* 'Samostojna izdelava programov' na eUčilnici.  *\n" +
                        "* Vse obveznosti predmeta Programiranje 2 bom    *\n" +
                        "* skladno s temi navodili opravil samostojno.    *\n" +
                        "**************************************************";
        String[] lines = izjava.split("\n", 6);

        System.out.println(ime);
        System.out.println(vpisna);
        System.out.println();
        for (int i = 0; i<6; i++){
            System.out.println(lines[i]);
        }
    }
}
