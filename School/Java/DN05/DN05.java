import java.io.File;
import java.util.*;


public class DN05{
	
	static char byte2char(byte letter){
		char val = (char) letter;
		return val;
	}
	
	
	public static void main(String[] args) throws Exception {
		String ime = args[0];
		Scanner file = new Scanner(new File(ime));
		
		//read 8bits and turn them into decimal...
		//while file has any characters
		//read 8 chars in a for loop, add them to a string
		//call bin2dec(string)
		//turn int to char.
		String text = "";
		while (file.hasNext()){
			byte letter = file.nextByte();
			text += byte2char(letter);
		}
		System.out.println(text);
	}
}