import java.util.*;
import java.text.DecimalFormat;

public class DN03{
	
	static void add(double op1, double op2){
		double result = op1 + op2;
		System.out.printf("%.1f + %.1f = %.1f\n", op1, op2, result);
	}
	
	static void sub(double op1, double op2){
		double result = op1 - op2;
		System.out.printf("%.1f - %.1f = %.1f\n", op1, op2, result);
	}
	
	static void mul(double op1, double op2){
		double result = op1 * op2;
		System.out.printf("%.1f * %.1f = %.1f\n", op1, op2, result);
	}
	
	static void div(double op1, double op2){
		double result = op1 / op2;
		System.out.printf("%.1f / %.1f = %.1f\n", op1, op2, result);
	}
	
	static void min(String[] args){
		double min = Double.MAX_VALUE;
		for (int i = 1; i <= args.length-1; i++){        //ta for zanka zbere iz seznama najmanjsi element
			if (Double.parseDouble(args[i]) <= min){
				min = Double.parseDouble(args[i]);
			}
		}
		
		DecimalFormat df = new DecimalFormat("#####.######");
		String fmin = df.format(min);                //pravilno formatiranje najmanjse cifre
		
		String allnums = arrtostr(args);             // string vseh stevilk
		
		System.out.printf("Minimum stevil " + allnums + " je %s\n", fmin);
	}
	
	static String arrtostr(String[] array){     //nardi iz arraya stevilk string
		String ret = "";
		for (String num : array){
			ret += num + " ";
		}
		return ret.substring(2, ret.length()-1);
	}
	
	public static void main(String[] args){
		Locale.setDefault(Locale.ENGLISH);

		double op1 = Double.parseDouble(args[1]);
		double op2 = Double.parseDouble(args[2]);

		if (args[0].equals("1")){
			add(op1, op2);
		}
		else if (args[0].equals("2")){
			sub(op1, op2);
		}
		else if (args[0].equals("3")){
			mul(op1, op2);
		}
		else if (args[0].equals("4")){
			div(op1, op2);
		}
		else if (args[0].equals("5")){
			min(args);
		}
	}
}