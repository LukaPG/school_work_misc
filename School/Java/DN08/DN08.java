
public class DN08{
	
	static void nextPalindrome(String number){

		if (number.length() == 1){
			if (number.equals("9")){
				System.out.println("11");
			} else {
				int num = Integer.parseInt(number)+1;
				System.out.println(num);
			}
		}

		else if (number.length() % 2 == 0){ //If number of digits is even
			//do stuff
			String firstHalf  = number.substring(0, number.length()/2);

			String secondHalf = number.substring((number.length()/2), number.length());

			String reversedFirstHalf = new StringBuilder(firstHalf).reverse().toString();

			if (Integer.parseInt(reversedFirstHalf) > Integer.parseInt(secondHalf)){
				System.out.println(firstHalf + reversedFirstHalf);
			}

			else if (Integer.parseInt(reversedFirstHalf) <= Integer.parseInt(secondHalf)) {
				int ifHalf = Integer.parseInt(firstHalf) + 1;
				String fHalf = String.format("%d", ifHalf);
				String reversedFHalf = new StringBuilder(fHalf).reverse().toString();
				System.out.println(fHalf + reversedFHalf);
			}

		}
		else if (number.length() % 2 == 1){ // if number of digits is odd
			//do some other stuff

			String firstHalf   = number.substring(0, number.length()/2);

			String secondHalf  = number.substring((number.length()/2)+1, number.length());

			String middleDigit = number.substring((number.length()/2), number.length()/2 + 1);

			String reversedFirstHalf = new StringBuilder(firstHalf).reverse().toString();

			if (Integer.parseInt(reversedFirstHalf) > Integer.parseInt(secondHalf)){
				System.out.println(firstHalf + middleDigit + reversedFirstHalf);
			}

			else if (Integer.parseInt(reversedFirstHalf) <= Integer.parseInt(secondHalf)) {
				int imDigi = Integer.parseInt(middleDigit) + 1;
				String mDigi = String.format("%d", imDigi);

				System.out.println(firstHalf + mDigi + reversedFirstHalf);
			}
		}
	}

	public static void main(String[] args){
		String stevilo = args[0];
		nextPalindrome(stevilo);
	}
}