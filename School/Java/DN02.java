public class DN02{
	static void pravokotnik(String x, String y){
		int y1 = Integer.parseInt(x);
		int x1 = Integer.parseInt(y);
		
		for (int i=0; i<y1; i++){
			if (i == 0){
					System.out.printf("a =%2d , b =%2d    ", y1, x1);
			}
			else{
				System.out.printf("%17s", " ");
			}
			for (int j=0; j<x1; j++){
				System.out.print("X");	
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args){
		if (args.length >= 2){
			pravokotnik(args[0], args[1]);
		}
		else{
			for (int i = 1; i <= 5; i++){
				for (int j = 1; j <= 5; j++){
					String x = Integer.toString(i);
					String y = Integer.toString(j);
					pravokotnik(x, y);
					System.out.println();
				}
			}
		}
	}
}