#!/bin/bash
#Luka Prebil Grintal Copyright 2016

if [[ "$#" -eq 0 ]]; then
	echo "Podajte argument"
	exit 1
fi
if [[ "$#" -gt 1 ]]; then
	num="$#"
	echo "St. arg. mora biti 1"
	exit "$num"
fi
string="$1"
if [[ ${#string} -eq 1 ]]; then
	echo "Argument mora biti daljsi od le enega znaka"
	exit 255
fi

reverse=$(rev <<< "$string")

if [[ "$reverse" == "$string" ]]; then
	echo "$string JE palindrom"
else
	echo "$string NI palindrom"
fi
